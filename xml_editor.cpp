//
// Created by Артем Автайкин on 19.04.2022.
//

#include "xml_editor.h"
using namespace tinyxml2;


const char * Xml_editor::get(const char * element) {
    XMLDocument doc;
    bool isLoad = doc.LoadFile("/Users/artyom_avtaykin/PycharmProjects/xml_editor/build/new.xml");
    //bool isLoad = doc.LoadFile("/xml_editor/ex.xml");
    if (isLoad == 0) {
        XMLText *textNode = doc.FirstChildElement("root")->FirstChildElement(element)->FirstChild()->ToText();
        const char * title = textNode->Value();
        std::cout << "Value: " << title << std::endl;
        return title;
    } else {
        std::cout << "Failed to load file" << std::endl;
        return 0;
    }
}

bool Xml_editor::set(const char* name, const char* content) {
    XMLDocument doc;
    bool isLoad = doc.LoadFile("/Users/artyom_avtaykin/PycharmProjects/xml_editor/build/ex.xml");
    //bool isLoad = doc.LoadFile("/xml_editor/ex.xml");
    if (isLoad == 0) {
        XMLElement* root = doc.FirstChildElement( "root" );
        XMLElement* element = doc.NewElement(name);
        XMLText * text = doc.NewText(content);
        element->LinkEndChild(text);
        root->LinkEndChild(element);
        //doc.SaveFile("/Users/artyom_avtaykin/CLionProjects/xml_editor/ex.xml");
        doc.SaveFile("/Users/artyom_avtaykin/PycharmProjects/xml_editor/build/ex.xml");
        std::cout << "Success" << std::endl;
        return 1;
    } else {
        std::cout << "Failed to load file" << std::endl;
        return 0;
    }
}

unsigned int Xml_editor::crc32b(std::string message) {
    int i, j;
    unsigned int byte, crc, mask;
    i = 0;
    crc = 0xFFFFFFFF;
    while (message[i] != 0) {
        byte = message[i];            // Get next byte.
        crc = crc ^ byte;
        for (j = 7; j >= 0; j--) {    // Do eight times.
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
        }
        i = i + 1;
    }
    return ~crc;
}
