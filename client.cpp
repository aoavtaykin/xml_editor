#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream>
#include "vector"
#include "xml_editor.h"

void error(const char *msg)
{
    perror(msg);
    exit(0);
}


int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[1024];
    if (argc < 3) {
        fprintf(stderr,"usage %s hostname port\n", argv[0]);
        exit(0);
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    Xml_editor* editor = new Xml_editor();
    if (sockfd < 0)
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
          (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
        error("ERROR connecting");
    int choice;
    std::cout << "Type the number of operation you want to do:" << std::endl << "1. Get data from database" << std::endl
    << "2. Put data in database" << std::endl;
    std::cin >> choice;
    if(choice == 1){
        bzero(buffer, 1024);
        std::string get_flag = "52454144h";
        //Xml_editor* editor = new Xml_editor();
        //std::cout << editor->crc32b(get_flag);
        n = write(sockfd, get_flag.data(), get_flag.size());
        if (n < 0)
            error("ERROR writing to socket");
        n = read(sockfd, buffer, 1024);
        if (n < 0)
            error("ERROR reading from socket");
        std::string s_buffer = buffer;
        std::string c_sum;
        std::string re_c_sum;
        int m_start;
        for(int i = 0; i < s_buffer.size(); i++){
            if (s_buffer[i] == 's' and s_buffer[i + 1] == 't'){
                re_c_sum = s_buffer.substr(0, i);
                std::cout << re_c_sum << std::endl;
                m_start = i + 5;
                break;
            }
        }
        std::cout << s_buffer.substr(m_start) << std::endl;
        c_sum = std::to_string(editor->crc32b(s_buffer.substr(m_start)));
        std::cout << c_sum << std::endl;
        if (c_sum == re_c_sum) {
            std::cout << std::endl;
            printf("%s\n", buffer);
        } else{
            error("Damaged file");
        }
    }else{
        if(choice == 2){
            std::string input;
            std::cout << "Enter data in format 'article value'" << std::endl;
            std::cin.ignore();
            getline(std::cin, input);
            //std::cout << input << std::endl;
            int pointer = 0;
            std::vector<std::string> ar_val;
            for(int i = 0; i < input.size(); i++){
                if(input[i] == ' ' or i == input.size() - 1){
                    ar_val.push_back(input.substr(pointer, i));
                    pointer = i + 1;
                }
            }
            //std::cout << ar_val[1] << std::endl;
            std::vector<std::string> article;
            std::vector<std::string> value;
            for(int i = 0; i < ar_val.size(); i++){
                if (i % 2 == 0){
                    article.push_back(ar_val[i]);
                } else{
                    value.push_back(ar_val[i]);
                }
            }
            //std::cout << article[0] << std::endl;
            FILE *in = fopen("ex.xml", "w");
            fprintf(in, "%s", "<root>\n \n</root>");
            fclose(in);
            if (article.size() == value.size()) {
                for (int i = 0; i < article.size(); i++) {
                    editor->set(article[i].data(), value[i].data());
                }
            } else{
                error("Input error");
            }
            bzero(buffer, 1024);
            std::string put_flag = "53415645h";
            in = fopen("ex.xml","r");
            if (in != NULL) {
                while (!feof(in)) {
                    int b = fread(buffer, 1, sizeof(buffer), in);
                    //printf("%s", buffer);
                    if (b != 0) {
                        std::string s_buffer = buffer;
                        std::string c_sam = std::to_string(editor->crc32b(s_buffer));
                        std::cout << c_sam << std::endl;
                        std::string beg_file = "start";
                        std::string message = put_flag + c_sam + beg_file + s_buffer;
                        n = write(sockfd, message.data(), message.size());
                        if (n < 0)
                            error("ERROR writing to socket");
                    }
                }
                fclose(in);
            } else{
                error("Error opening file");
            }
        } else{
            error("Wrong number");
        }
    }

    close(sockfd);
    return 0;
}

