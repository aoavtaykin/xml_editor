#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include "vector"
#include "xml_editor.h"
#include "db.h"

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
    int sockfd, newsockfd, portno;
    socklen_t clilen;
    char buffer[1024];
    struct sockaddr_in serv_addr, cli_addr;
    int n;
    Xml_editor* editor = new Xml_editor();
    db* n_db = new db();
    std::vector<std::string> columns =  {"ARTICLE       TEXT      NOT NULL", "VALUE           TEXT    NOT NULL"};
    n_db->createTable("XML_DATA", columns);
    if (argc < 2) {
        fprintf(stderr,"ERROR, no port provided\n");
        exit(1);
    }
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");
    bzero((char *) &serv_addr, sizeof(serv_addr));
    portno = atoi(argv[1]);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr,
             sizeof(serv_addr)) < 0)
        error("ERROR on binding");
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
    newsockfd = accept(sockfd,
                       (struct sockaddr *) &cli_addr,
                       &clilen);
    if (newsockfd < 0)
        error("ERROR on accept");
    bzero(buffer,1024);
    n = read(newsockfd,buffer, 1024);
    if (n < 0) error("ERROR reading from socket");
    std::string s_buffer = buffer;
    if(s_buffer.substr(0, 9) == "53415645h") {
        std::string c_sum;
        std::string re_c_sum;
        int m_start;
        for(int i = 9; i < s_buffer.size(); i++){
            if (s_buffer[i] == 's' and s_buffer[i + 1] == 't'){
                re_c_sum = s_buffer.substr(9, i - 9);
                std::cout << re_c_sum << std::endl;
                m_start = i + 5;
                break;
            }
        }
        std::cout << s_buffer.substr(m_start) << std::endl;
        c_sum = std::to_string(editor->crc32b(s_buffer.substr(m_start)));
        std::cout << c_sum << std::endl;
        if (c_sum == re_c_sum) {
            FILE *in = fopen("new.xml", "w");
            bzero(buffer, 1024);
            strcpy(buffer, s_buffer.substr(m_start).data());
            fprintf(in, "%s", buffer);
            fclose(in);
            //printf("Here is the message: %s\n", buffer);
            std::string a_name = "article";
            const char *get_v = a_name.data();
            std::string value = editor->get(get_v);
            std::stringstream ss1;
            ss1 << "'" << a_name << "'";
            std::stringstream sss;
            sss << "'" << value << "'";
            std::string str = sss.str();
            std::vector<std::string> columns = {"ARTICLE", "VALUE"};
            std::vector<std::string> values = {ss1.str(), sss.str()};
            n_db->insert("XML_DATA", columns, values);
        } else{
            error("Damaged message");
        }
    }
    if(s_buffer.substr(0, 9) == "52454144h") {
        std::string s_buf = "";
        std::vector<std::string> selected = n_db->select("XML_DATA", {});
        for(int i = 0; i < selected.size(); i++){
            s_buf = s_buf + selected[i];
        }
        std::cout << s_buf << std::endl;
        std::string c_sum = std::to_string(editor->crc32b(s_buf));
        std::string m_start = "start";
        std::string message = c_sum + m_start + s_buf;
        n = write(newsockfd, message.data(), s_buf.size());
        if (n < 0)
            error("ERROR writing to socket");

    }
    close(newsockfd);
    close(sockfd);
    return 0;
}

